/* global Package */

Package.describe({
  name: 'amoreno:ddpauthplus',
  version: '1.1.6',
  summary: 'This package is useful for multiple apps centralized login.',
  git: 'https://gitlab.com/adrian.flda/ddpauthplus.git',
  documentation: 'README.md'
})

Package.onUse(function (api) {
  api.versionsFrom('1.6')

  api.use([
    'ecmascript',
    'check',
    'mongo',
    'accounts-base',
    'ddp'
  ])

  api.mainModule('server/index.js', 'server')
  api.mainModule('client/index.js', 'client')
})
