import { Meteor } from 'meteor/meteor'
import Provider from './Provider'
import Client from './Client'

__meteor_runtime_config__.ACCOUNTS_CONNECTION_URL = Meteor.settings.DDPAuth.providerLoginURL

export const providerLoginConnection = DDP.connect(__meteor_runtime_config__.ACCOUNTS_CONNECTION_URL);

let providerInstance = null

const DDPAuth = {
  startProvider (additionalSelectCriteria) {
    if (providerInstance) {
      throw new Meteor.Error('DDPAuth provider can be started only once!')
    }

    providerInstance = new Provider(additionalSelectCriteria)
    providerInstance.start()
    return providerInstance
  },

  startClient (connection) {
    const instance = new Client(connection)
    instance.start()
    return instance
  },
}

export {
  DDPAuth
}

Meteor.startup(function () {
  switch (Meteor.settings.DDPAuth.type) {
    case "client":{
      DDPAuth.clientSide = DDPAuth.startClient(providerLoginConnection)
      break;
    }
    case "provider":{
      DDPAuth.providerSide = DDPAuth.startProvider()
      break;
    }
    default:
  }
})
